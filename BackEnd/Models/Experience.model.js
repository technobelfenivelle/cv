
class Experience
{
    constructor(titre,annee)
    {
        this.titre=titre;
        this.annee=annee;
    } 

    get Titre() {
        return this.titre;
      } 
    
    get Annee()
    {
        return this.annee;
    }

    fake()
    {
        let listExperience = [];

        listExperience.push(new Experience("Formateur Technobel", 2020));
        listExperience.push(new Experience("Apprenti patissier", 2020));
        listExperience.push(new Experience("Customiseur de GameBoy", 1997));
        listExperience.push(new Experience("Rebobinneur de casset audio", 1988));
        return listExperience; 
    }
}



export default Experience;