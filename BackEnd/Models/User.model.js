import  Experience from "./Experience.model.js";
 
class User
{
    constructor(nom,prenom)
    {
        this.nom=nom;
        this.prenom=prenom;
        
        this.experiences=new Experience().fake();
    } 

    get Nom()
    {
        return this.nom;

    }

    get Prenom()
    {
        return this.prenom;
    }

    get MesExperiences() 
    {
        return this.experiences;
    }
}

export default User;