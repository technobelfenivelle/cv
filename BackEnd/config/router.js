

//importer express
import Express from 'express'  
import GenerateExcel from '../Modules/excel-buffer/excelwrapper.js'
import userService from '../services/User.service.js'; 
//Déclaration de l'objet router
const Router = Express.Router();
Router.get("/",async function(req, res)
    { 
        res.redirect("./swagger") ;
         
    }); 

//Définition des mes routes
//Html
/**
 * @swagger
 * 
 * /:
 *   get:
 *     description: Affiche le nom et le prénom
 *     produces:
 *       - text/html
 *     responses:
 *       200:
 *         description: Récupération du nom et du prénom
 * 
 */
Router.get("/api",async function(req, res)
    {
        let u = await new userService().GetUsers(); 
        res.send('<h1>'+u.Nom+' '+u.Prenom +'</h1>');    
         
    });  

// GET /cv
/**
 * @swagger
 * 
 * /cv:
 *   get:
 *     description: Générer un fichier excel contenant les informations du cv
 *     produces:
 *       - application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
 *     responses:
 *       200:
 *         description: Générer un fichier excel contenant les informations du cv
 */
Router.get("/api/cv",async function(req, res)
    {
        let u = await new userService().GetUsers(); 
      
 
        let col = ['Nom','Prenom']; 
        for(const el in u.MesExperiences)
        {
            col.push("Experience" + el);
        }

        let data = [u.Nom,u.Prenom];
        for(const el of u.MesExperiences)
        {
            
            data.push('('+el.Annee+')'+el.Titre);
        }

        let buffer =GenerateExcel(col,data);

        res.writeHead(200, {'Content-Type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64'
        ,"X-Content-Type-Options" :"nosniff",
        "Content-Disposition": "attachment; filename*=UTF-8''fichier.xlsx"});
        res.end(Buffer.from(buffer, 'base64'));   
         
    }); 

// GET /Experiences
/**
 * @swagger
 *
 * /Experiences:
 *   get:
 *     description: Récupère la liste des experiences par années
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Les expériences prof. par années
 */
Router.get("/api/Experiences",async function(req, res)
    {
        let u = await new userService().GetUsers(); 
        res.jsonp(u.MesExperiences);    
         
    });  
// GET /Experiences/:annee
/**
 * @swagger
 *
 * /Experiences/{annee}:
 *   get:
 *     description: Récupère les expériences d'une année
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: annee
 *         description: l'année des experiences recherchées
 *         in: path 
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Récupère les expériences de l'année demandée
 */
Router.get("/api/Experiences/:annee",async function(req, res)
    {
        let u = await new userService().GetUsers(); 
        let exp = u.MesExperiences.find(e=>e.Annee==req.params.annee);

        res.jsonp(exp);    
         
    }); 
export default Router;