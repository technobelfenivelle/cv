const options = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "CV Express API with Swagger",
        version: "0.1.0",
        description:
          "This is a simple CRUD API application made with Express and documented with Swagger",
        license: {
          name: "MIT",
          url: "https://spdx.org/licenses/MIT.html",
        },
        contact: {
          name: "Mike Person",
          url: "https://www.cognitic.be",
          email: "michael.person@cognitic.be",
        },
      },
      servers: [
        {
          url: "http://localhost:5264/api",
        },
      ],
    },
    apis: ["../Models/User.model.js","./config/router.js"],
  };
  
  

  export default options