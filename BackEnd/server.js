 //importer express
 import Express from 'express'
 //importer notre module de routage
 import router from './config/router.js'
 //importe le module cors pour les requêtes multi origin
 import cors from 'cors'
 //import des modules pour la documentation de l'api
 import swaggerJSDoc from 'swagger-jsdoc'
 import options from "./config/swagger.js"
 import swaggerUi from 'swagger-ui-express'


 //Déclarer ma "constante" applicative express
 const app = Express();

 //Utiliser le middleware cors pour toutes les routes
 app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

 //Dire que j'utilise un router 
    //Je veux utiliser router.js ==> Ajout en tant que middleware
    app.use("/", router);

//Documentation de l'api via swagger ui
const swaggerSpec = swaggerJSDoc(options);
console.log(swaggerSpec); 
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec, { explorer: true }));

// On lance le server
app.listen(5264,()=>console.log("Serveur lancé sur http://localhost:5264"));
