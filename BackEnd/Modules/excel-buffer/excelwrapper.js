
import xlsx from 'xlsx'
const GenerateExcel=(col,data)=>
{
    let wb = xlsx.utils.book_new();
    var table = [col, data];
        console.log(table);
        
        var ws = xlsx.utils.aoa_to_sheet(table);
        xlsx.utils.book_append_sheet(wb, ws, 'Donnees');

        // write options
        const wopts = { bookType: 'xlsx', bookSST: false, type: 'base64' };
        const buffer = xlsx.write(wb, wopts);

        return buffer;
}

export default GenerateExcel;