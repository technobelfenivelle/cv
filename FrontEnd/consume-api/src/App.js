import './App.css';
import {BrowserRouter as BRouter ,
  Switch,Link,
  Route} from 'react-router-dom';
import DataViewer from './Components/DataViewer/DataViewer';
import Menu from "./Components/Menu/Menu";
import InfoUser from './Components/InfoUser/InfoUser';
import DownloadFile from './Components/DownloadFile/DownloadFile';
import Recherche from './Components/Recherche/Recherche';

function App() {
  return (
    <div className="App">
      
      <header className="App-header">
          <h1>CV api</h1>
      </header>
      <BRouter>
     <Menu/>
        
      <Switch>
          <Route path="/Home" > 
             <h1>Bienvenue chez  <InfoUser path=""/> </h1>
          </Route>
          <Route path="/MesExperiences">
            <h1>Mes experiences</h1>
              <DataViewer path="experiences"/>
          </Route>
          <Route path="/CvExcel">
              <h1>Fichier excel</h1>
              <br/>
              <DownloadFile path="cv"/>
          </Route>
          <Route path="/Search">
              <h1>Recherche </h1>
              <br/> 
              <Recherche/>
          </Route>
        </Switch> 
      </BRouter>   
    </div>
  );
}

export default App;
