import React, { useState, useEffect } from 'react' 
//import Axios from 'axios'
import ApiRequester from '../../Modules/ApiRequester/api-requester';
const ApiUrl = "http://localhost:5264/api";

const InfoUser = (props)=>
{    
    const {path} =props; 
    const Requester = new ApiRequester(ApiUrl+"/"+path);
    const [info, setInfo] = useState(""); 
    useEffect(() => {
        
         Requester.MakeRequest()
        .then((e)=>setInfo(e));
         
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [path]);
    return (
        <p dangerouslySetInnerHTML={{__html: info}}></p>
      
    );
}

export default InfoUser;