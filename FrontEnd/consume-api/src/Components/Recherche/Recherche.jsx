import React, {useState} from 'react' 
import ApiRequester from '../../Modules/ApiRequester/api-requester.js'
import JSONViewer from 'react-json-viewer' 
const ApiUrl = "http://localhost:5264/api";

const Recherche = (props)=>
{     
    const [Annee,setAnnee] = useState(2020);
    const [data, setData] = useState(null);
    function search()
    {
        const Requester = new ApiRequester(ApiUrl+"/experiences/"+Annee);
        Requester.MakeRequest()
        .then((e)=>setData(e));

    }

    function handleInput(e)
    {
        const { value} = e.target;
        setAnnee(value);
    } 

    return (
        <div>
       Année : <input type="text" name="Annee" value={Annee} onChange={handleInput}></input><br/>
       <button onClick={search}>Rechercher une experience</button>
       
        <JSONViewer
        json={data}
        />
      </div>
    );
}

export default Recherche;