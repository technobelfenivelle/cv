import React from 'react' 
const ApiUrl = "http://localhost:5264/api";

const DownloadFile = (props)=>
{    
    const {path} =props; 
    
    return (
       
        <a className="btn btn-default" href={ApiUrl+"/"+path}>Download</a>
      
    );
}

export default DownloadFile;