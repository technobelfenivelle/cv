import React, { useState, useEffect } from 'react'
import JSONViewer from 'react-json-viewer' 
//import Axios from 'axios'
import ApiRequester from '../../Modules/ApiRequester/api-requester';
const ApiUrl = "http://localhost:5264/api";

const DataViewer = (props)=>
{    
    const {path} =props;
    console.log(path);
    const Requester = new ApiRequester(ApiUrl+"/"+path);
    const [data, setData] = useState(null);
    useEffect(() => {
        
         Requester.MakeRequest()
        .then((e)=>setData(e));
         
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [path]);
    return (
        <JSONViewer
        json={data}
      />
    );
}

export default DataViewer;