import {Link} from 'react-router-dom'; 
export default function Menu (props)
{
    return( 
        <nav className="navbar navbar-default">
        <div className="container-fluid"> 
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="/">React-Express</a>
          </div>
      
           <div className="collapse navbar-collapse" id="navbar-collapse">
            <ul className="nav navbar-nav">
              <li><Link to="/Home">Home <span className="sr-only">(current)</span></Link></li>
              <li><Link to="/MesExperiences">Mes experiences (json)</Link></li> 
              <li><Link to="/CvExcel">Télécharger l'excel</Link></li>    
              <li><Link to="/Search">Rechercher</Link></li>         
           </ul>
          </div> 
        </div> 
      </nav> 
    );
}