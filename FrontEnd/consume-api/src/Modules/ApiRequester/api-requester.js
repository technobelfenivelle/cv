import Axios from 'axios'

class ApiRequester
{
    constructor(Url)
    { 
        this.client = Axios.create({
            baseURL: Url
          });
    }

    MakeRequest(options)
    { 
        return this.client(options)
            .then(this.onSuccess)
            .catch(this.onError);
            
    }

    onSuccess = (response) =>
    {
        console.debug('Request Successful!', response);
        return response.data;
    }
    onError = (error)=>
    {
        console.error('Request Failed:', error.config);
    
        if (error.response) 
        {
            
              console.error('Status:',  error.response.status);
              console.error('Data:',    error.response.data);
              console.error('Headers:', error.response.headers);
        
        }
        else 
        { 
         console.error('Error Message:', error.message);
        }
        
        return Promise.reject(error.response || error.message);
    }
}

export default ApiRequester;